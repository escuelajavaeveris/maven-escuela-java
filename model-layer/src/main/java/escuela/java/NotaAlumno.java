package escuela.java;

/**
 * Created by hgranjal on 11/06/2018.
 */
public class NotaAlumno {

    private String nombre;
    private int nota;

    public NotaAlumno(String nombre, int nota) {
        this.nombre = nombre;
        this.nota = nota;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }
}
