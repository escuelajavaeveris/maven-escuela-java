package escuela.java;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hgranjal on 11/06/2018.
 */
public class HelloServlet extends HttpServlet {

    List<NotaAlumno> notasAlumnos = new ArrayList<>();

    @Override
    public void init() throws ServletException {
        NotaAlumno notaPatrici = new NotaAlumno("Patrici", 10);
        NotaAlumno notaLorena = new NotaAlumno("Lorena", 12);
        NotaAlumno notaCarla = new NotaAlumno("Carla", 15);
        notasAlumnos.add(notaPatrici);
        notasAlumnos.add(notaLorena);
        notasAlumnos.add(notaCarla);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        out.println("<h1>Hello world!</h1>");
        for (NotaAlumno notaAlumno : notasAlumnos) {
            out.println("<p>" + notaAlumno.getNombre() + " : " + notaAlumno.getNota() + "</p>");
        }
        out.println("<h1>" + CalculoNotas.calcularMedia(notasAlumnos) + "</h1>");
    }

    protected List<NotaAlumno> scaleNotas(List<NotaAlumno> notasAlumnos, int scale) {
        for (NotaAlumno notaAlumno: notasAlumnos) {
            notaAlumno.setNota(notaAlumno.getNota()*scale);
        }
        return notasAlumnos;
    }
}
